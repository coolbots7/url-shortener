var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var geoip = require('geoip-lite');
var UAParser = require('ua-parser-js');
var parser = new UAParser();
var router = express.Router();

var dbSchema = `
     CREATE TABLE IF NOT EXISTS t_Urls (
          URL_ID integer,
          Code text NOT NULL,
          URL text NOT NULL,
          Hits INTEGER NOT NULL DEFAULT 0,
          CreatedOn DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
          RemoteAddress TEXT NOT NULL,
          BrowserName TEXT NOT NULL,
          BrowserVersion TEXT NOT NULL,
          Country TEXT NOT NULL,
          Region TEXT NOT NULL,
          EU BOOLEAN NOT NULL,
          TimeZone TEXT NOT NULL,
          City TEXT NOT NULL,
          Latitude DOUBLE NOT NULL,
          Longitude DOUBLE NOT NULL,
          Metro INTEGER NOT NULL,
          Area INTEGER NOT NULL,
          PRIMARY KEY(URL_ID),
          UNIQUE(Code)
     );

     CREATE TABLE IF NOT EXISTS t_Hits (
          URL_ID INTEGER NOT NULL,
          CreatedOn DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
          RemoteAddress TEXT NOT NULL,
          BrowserName TEXT NOT NULL,
          BrowserVersion TEXT NOT NULL,
          Country TEXT NOT NULL,
          Region TEXT NOT NULL,
          EU BOOLEAN NOT NULL,
          TimeZone TEXT NOT NULL,
          City TEXT NOT NULL,
          Latitude DOUBLE NOT NULL,
          Longitude DOUBLE NOT NULL,
          Metro INTEGER NOT NULL,
          Area INTEGER NOT NULL,
          FOREIGN KEY (URL_ID) REFERENCES t_Urls(URL_ID)
     );
`;

let db = new sqlite3.Database('./url-shortener.db', (err) => {
     if (err) {
          console.error(err.message);
     }
     console.log('Connected to the url-shortener database.');

     db.exec(dbSchema, function(err) {
          if (err) {
               console.log(err);
          }
     });
});

/* GET home page. */
router.get('/', function(req, res, next) {
     res.render('index', {
          title: 'Express'
     });
});

router.get('/all', function(req, res, next) {
     getUrls().then(function(shortened_urls) {
          res.render('all', {
               title: 'Express',
               shortened_urls: shortened_urls
          });
     }, function(err) {
          console.log(err);
     });

});

router.post('/', function(req, res, next) {
     console.table(req.body);


     var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
     var geo = geoip.lookup(ip);
     var ua = req.headers['user-agent'];
     var browserName = parser.setUA(ua).getBrowser().name;
     var fullBrowserVersion = parser.setUA(ua).getBrowser().version;
     var browserVersion = fullBrowserVersion.split(".", 1).toString();
     var browserVersionNumber = Number(browserVersion);

     console.log("ip: " + ip);
     console.log("x-forwarded-for: " + req.headers['x-forwarded-for']);
     console.log("geo");
     console.table(geo);
     console.log("browserName: " + browserName);
     console.log("fullBrowserVersion: " + fullBrowserVersion);
     console.log("browserVersion: " + browserVersion);
     console.log("browserVersionNumber: " + browserVersionNumber);


     var code = generateCode(6);
     //TODO Make sure code is unique, if not regenerate

     var url = req.body.url;

     var sql = `INSERT INTO t_Urls (
          Code,
          URL,
          RemoteAddress,
          BrowserName,
          browserVersion,
          Country,
          Region,
          EU,
          TimeZone,
          City,
          Latitude,
          Longitude,
          Metro,
          Area
     ) VALUES (
          '` + code + `',
           '` + req.body.url + `',
           '` + ip + `',
           '` + browserName + `',
           '` + fullBrowserVersion + `',
           '` + geo["country"] + `',
           '` + geo["region"] + `',
           ` + geo["eu"] + `,
           '` + geo["timezone"] + `',
           '` + geo["city"] + `',
           ` + geo["ll"][0] + `,
           ` + geo["ll"][1] + `,
           ` + geo["metro"] + `,
           ` + geo["area"] + `);`;

     console.log("SQL: " + sql);

     db.exec(sql, function(err) {
          if (err) {
               console.log(err);
               res.redirect('/');
          }

          res.render('index', {
               title: 'Express',
               short_url: 'https://l.coolbots7.io/' + code
          });
     });
});

router.get('/:id', function(req, res, next) {
     var id = req.params.id;

     getUrl(id).then(function(result) {
          if (result.length > 0) {
               var url = result[0].URL;
               var id = result[0].URL_ID

               var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
               var geo = geoip.lookup(ip);
               var ua = req.headers['user-agent'];
               var browserName = parser.setUA(ua).getBrowser().name;
               var fullBrowserVersion = parser.setUA(ua).getBrowser().version;
               var browserVersion = fullBrowserVersion.split(".", 1).toString();
               var browserVersionNumber = Number(browserVersion);

               console.log("ip: " + ip);
               console.log("x-forwarded-for: " + req.headers['x-forwarded-for']);
               console.log("geo");
               console.table(geo);
               console.log("browserName: " + browserName);
               console.log("fullBrowserVersion: " + fullBrowserVersion);
               console.log("browserVersion: " + browserVersion);
               console.log("browserVersionNumber: " + browserVersionNumber);

               sql = `UPDATE t_Urls SET Hits = Hits + 1 WHERE URL_ID = ` + id + `;`;
               if (geo != null) {

                    sql += `INSERT INTO t_Hits (
               URL_ID,
               RemoteAddress,
               BrowserName,
               browserVersion,
               Country,
               Region,
               EU,
               TimeZone,
               City,
               Latitude,
               Longitude,
               Metro,
               Area) VALUES (
               ` + id + `,
               '` + ip + `',
               '` + browserName + `',
               '` + fullBrowserVersion + `',
               '` + geo["country"] + `',
               '` + geo["region"] + `',
               ` + geo["eu"] + `,
               '` + geo["timezone"] + `',
               '` + geo["city"] + `',
               ` + geo["ll"][0] + `,
               ` + geo["ll"][1] + `,
               ` + geo["metro"] + `,
               ` + geo["area"] + `);`;
               }

               console.log("sql: " + sql);

               db.exec(sql, function(err) {
                    if (err) {
                         console.log(err);
                    }
                    console.log(url);
                    res.set('location', url);
                    res.header("Pragma", "no-cache");
                    res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
                    res.status(301).send();
               });

          } else {
               console.log("not found");
               res.render('index', {
                    title: 'Express'
               });
          }
     }, function(err) {
          console.log(err);
     });

});

router.get('/:id/log', function(req, res, next) {
     var id = req.params.id;
     getUrlLog(id).then(function(results) {
          res.render('log', {
               title: 'MicroURL - ' + id + ' Log',
               code: id,
               log: results
          });
     }, function(err) {
          console.log(err);
     });
});

function getUrl(code) {
     return new Promise(function(resolve, reject) {
          db.all("SELECT * FROM t_Urls WHERE Code = '" + code + "'", [], (err, rows) => {
               if (err) {
                    reject(err);
               }
               resolve(rows);
          });
     });
}

function getUrls() {
     return new Promise(function(resolve, reject) {
          db.all("SELECT * FROM t_Urls", [], (err, rows) => {
               if (err) {
                    reject(err);
               }
               resolve(rows);
          });
     });
}

function getUrlLog(code) {
     return new Promise(function(resolve, reject) {
          db.all("SELECT * FROM t_Hits WHERE URL_ID = (SELECT URL_ID FROM t_Urls WHERE code = '" + code + "')", [], (err, rows) => {
               if (err) {
                    reject(err);
               }
               resolve(rows);
          });
     });
}

function generateCode(length) {
     var text = "";
     var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

     for (var i = 0; i < length; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

     return text;
}

module.exports = router;
