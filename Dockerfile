FROM node:10.15.1

# Install nodemon globally
RUN npm i nodemon -g

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY app.js .
COPY views ./views
COPY routes ./routes
COPY public ./public
COPY bin ./bin
RUN ln -sf /var/lib/url-shortener/url-shortener.db ./url-shortener.db

CMD [ "nodemon", "start" ]
